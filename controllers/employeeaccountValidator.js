const { check } = require('express-validator');

exports.addValidator = [
                        check('employeeaccountName',"กรอกชื่อผู้ใชไม่ถูกต้อง").not().isEmpty(),
                        check('username',"กรอกไอดีผู้ใช้ไม่ถูกต้อง").not().isEmpty(),
                        check('password',"กรอกรหัสผ่านไม่ถูกต้อง").not().isEmpty()];

exports.updateValidator = [
                        check('employeeaccountName',"กรอกชื่อผู้ใชไม่ถูกต้อง").not().isEmpty(),
                        check('username',"กรอกไอดีผู้ใช้ไม่ถูกต้อง").not().isEmpty(),
                        check('password',"กรอกรหัสผ่านไม่ถูกต้อง").not().isEmpty()];
