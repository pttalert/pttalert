const controller = {};
const { validationResult } = require('express-validator');

controller.dochistoryList = (req,res)=>{
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn)=>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query("SELECT DATE_FORMAT(DATE_ADD(datestam, INTERVAL 543 YEAR),'%d/%m/%Y')hwtsm, h.historywho_id as hwname, h.idhistory as idhistory, h.docnumber as hnum, h.docname hname, h.docrenewaldate hrene, h.docexpirationdate hexpir, h.doccontactagency hcy, h.doccontactperson hon, h.doccontactnumber her, h.docscan han, h.carbrand hnd, h.carnumber hmber, h.typedoc hdoc, h.bill hbl, h.brach hbra, h.detial hal, h.typecarhit htch FROM history as h ;",(err,data)=>{
          res.render("history/historyList",{session:req.session,data:data,data1:data1})
        })
      })
    })
  }else {
    res.redirect('/');
  };
}

controller.dochistoryAdd = (req,res) =>{
  if (req.session.admin || req.session.user){
    const {id} = req.params;
    req.getConnection((err,conn)=>{
      conn.query("select * FROM historywho;",(err,who)=>{
        conn.query("SELECT   dcm.unitmy as unitmy,dcm.unitmy as unitmy, dcm.billing_id as billidd, dcm.iddocument id,dcm.documentNumber as docnum, dcm.documentName as docname, DATE_FORMAT(documentRenewaldate, '%Y-%m-%d')dcmrene, DATE_FORMAT(documentExpirationdate, '%Y-%m-%d')dcmexpir, dcm.documentContactagency as dcmcy, dcm.documentContactperson as dcmson, dcm.documentContactnumber as dcmconnum, dcm.documentScan as dcmscan, dcm.Carbrand as dcmcarbrand, dcm.Carnumber as dcmcarnumber, tdm.typedocumentName as tdmname, bll.BillingName as bllname, brh.branchName as brhname, dcm.detial as dcmdetial, tyc.namecar as tycname  FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id LEFT JOIN typecar as tyc ON tyc.idtypecar=dcm.typecar WHERE iddocument = ?",[id],(err,data2)=>{
          conn.query("SELECT * FROM typedocument;",(err,data1)=>{
            res.render("history/historyAdd",{session:req.session,data1:data1,data2:data2[0],who:who});
          })
        })
      })
    })
  }else {
    res.redirect('/');
  };
}

controller.dochistorySave = (req,res) => {
  if (req.session.admin || req.session.user){
    const data2 = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/dochistory/dochistoryAdd/'+ data2.id);
    }else{
      req.getConnection((err,conn)=>{const errors = validationResult(req);
        if (!errors.isEmpty()) {
          req.session.errors = errors;
          req.session.success = false;
        } else {
          req.session.success=true;
          req.session.topic="ดำเนินการเสร็จแล้ว";
          if (data2.billid == "") { data2.billid = 0; }
          if (data2.brach == "") { data2.brach = 0; }
          if (data2.historyunit == "") { data2.historyunit = 0; }
          if (data2.doccontactperson == "") { data2.doccontactperson = 0; }
          if (data2.doccontactnumber == "") { data2.doccontactnumber = 0; }
          if (data2.carbrand == "") { data2.carbrand = 0; }
          if (data2.typecarhit == "") { data2.typecarhit = 0; }
          if (data2.historyunit == "") { data2.historyunit = 0; }
          conn.query("INSERT INTO history set docnumber = ? , docname = ? , docrenewaldate = ?,docexpirationdate = ?, doccontactagency = ?, doccontactperson = ?, doccontactnumber = ?, docscan = ?, carbrand = ?, carnumber = ?, typedoc = ?, bill = ?,brach = ?, detial = ?, typecarhit = ?, historywho_id = ?, historyunit = ?, billid = ?;",[data2.docnumber,data2.docname,data2.docrenewaldate,data2.docexpirationdate,data2.doccontactagency,data2.doccontactperson,data2.doccontactnumber,data2.docscan,data2.carnumber,data2.carbrand,data2.typedoc,data2.bill,data2.brach,data2.detial,data2.typecarhit,data2.historywho_id,data2.historyunit,data2.billid],(err,data1)=>{

            if (data2.idbill == 2){
              conn.query("SELECT DATE_ADD(?,INTERVAL ? MONTH) as new",[data2.docexpirationdate,data2.historyunit],(err,enddate) =>{
                conn.query("UPDATE document SET documentExpirationdate = ? , documentRenewaldate = ? WHERE iddocument = ?",[enddate[0].new,data2.docexpirationdate,data2.id],(err,newdate) =>{
                  res.redirect("/home");
                })
              })
            } else if (data2.idbill == 3){
              conn.query("SELECT DATE_ADD(?,INTERVAL ? year) as new",[data2.docexpirationdate,data2.historyunit],(err,enddate) =>{
                conn.query("UPDATE document SET documentExpirationdate = ? , documentRenewaldate = ? WHERE iddocument = ?",[enddate[0].new,data2.docexpirationdate,data2.id],(err,newdate) =>{
                  res.redirect("/home");
                })
              })
            }else if (data2.idbill == 1){
              conn.query("SELECT DATE_ADD(?,INTERVAL ? DAY) as new",[data2.docexpirationdate,data2.historyunit],(err,enddate) =>{
                conn.query("UPDATE document SET documentExpirationdate = ? , documentRenewaldate = ? WHERE iddocument = ?",[enddate[0].new,data2.docexpirationdate,data2.id],(err,newdate) =>{
                  res.redirect("/home");
                })
              })
            } else {
              conn.query("DELETE FROM document where iddocument = ?;",[data2.id],(err,data3)=>{
                res.redirect('/home');
              })
            }
          })
        }
      })
    }
  }else {
    res.redirect('/');
  };
}

controller.dochistoryDel = (req,res) => {
  if (req.session.admin || req.session.user){
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query("SELECT * FROM history WHERE idhistory = ?;",[id],(err,data)=>{
          res.render("history/historyDelete",{session:req.session,data:data[0],data1:data1})
        })
      })
    })
  }else {
    res.redirect('/');
  };
}

controller.dochistoryDelete = (req,res) =>{
  if (req.session.admin || req.session.user){
    const {id} = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
    } else {
      req.session.success=true;
      req.session.topic="ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn) =>{
        conn.query("DELETE FROM history WHERE idhistory = ?",[id], (err,data) =>{
          res.redirect("/dochistory/dochistoryList");
        })
      })
    }
  }else {
    res.redirect('/');
  };
}

module.exports = controller;
