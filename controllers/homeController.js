const controller ={};

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    var data2 = [{typedocumentName:"เอกสาร"}] ;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query(' SELECT   dcm.detial as detil, dcm.typecar as carrr, dcm.select_document, dcm.branch_id,dcm.billing_id,dcm.typedocument_id,dcm.Carnumber,dcm.Carbrand ,documentRenewaldate as eee, documentExpirationdate as asa,  dcm.documentContactagency as dcmconty, dcm.branch_id as dcmbrach, dcm.billing_id as dcmbil, dcm.typedocument_id as dcmtye, dcm.documentContactnumber as dcmvvv, dcm.documentNumber as dcmnu, bll.idBilling as blling, dcm.iddocument,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname, DATE_FORMAT(DATE_ADD(documentExpirationdate, INTERVAL 543 YEAR)," %d/%m/%Y")ddd FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE select_document = 0 ORDER BY documentExpirationdate ASC limit 10; ',(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('home/homeList',{session:req.session,data:data1,data1:data,data2:data2});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.listdate = (req,res) => {
  if (req.session.admin || req.session.user){
    const {year} = req.params;
    const {month} = req.params;
    const {date} = req.params;
    const {id} = req.params;
    console.log(year);
    var a = year;
    var b = month;
    var c = date;
    var dd = a + '-' + b + '-' + c;
    console.log(dd);
    req.getConnection((err,conn) =>{
      conn.query("SELECT * FROM typedocument;",(err,data1) =>{
        conn.query("SELECT dcm.iddocument,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(documentExpirationdate,'%d/%m/%Y')ddd FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE select_document = 0 and documentExpirationdate  = ? ORDER BY documentExpirationdate ASC ;",[dd], (err,data) =>{
          console.log(data);
          if(err){
            res.json(err);
          }
          res.render("document/documentList",{session:req.session,data1:data1,data:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
}


module.exports = controller;
