const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM employee as epy LEFT JOIN position as pst ON epy.employeeposition_id=pst.idposition LEFT JOIN branch as brn ON epy.employeebranch_id = brn.idbranch',(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('employee/employeeList',{session:req.session,data:data1});
      });
    });
};

controller.add = (req,res) => {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM position;', (err,data) => {
              conn.query('SELECT * FROM branch;', (err,data1) => {
                    res.render('employee/employeeAdd',{ session:req.session,data:data,data1:data1 });
              });
          });
      });
  };

controller.save = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/employee/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                conn.query('INSERT INTO employee set ?',[data],(err,data1)=>{
                    res.redirect('/employee');
                });
            });
         }
     };

controller.del = (req,res) => {
    const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM employee as epy LEFT JOIN position as pst ON epy.employeeposition_id=pst.idposition LEFT JOIN branch as brn ON epy.employeebranch_id = brn.idbranch WHERE idemployee = ?',[id],(err,data1)=>{
            if(err){
                res.json(err);
            }else {
              res.render('employee/employeeDelete',{session:req.session,data:data1[0]});
            }
        });
    });
};

controller.delete =(req,res) => {
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
    } else {
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM employee WHERE idemployee = ?',[id], (err,data1)=>{
              res.redirect('/employee');
          });
      });
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
    }
};

controller.edit = (req,res) => {
  const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM employee as epy LEFT JOIN position as pst ON epy.employeeposition_id=pst.idposition LEFT JOIN branch as brn ON epy.employeebranch_id = brn.idbranch WHERE idemployee = ?',[id],(err,data1) =>{
       conn.query('SELECT * FROM position;',(err,data2)=>{
        conn.query('SELECT * FROM branch;',(err,data3)=>{
        if(err){
          res.json(err);
        }
        res.render('employee/employeeUpdate',{ session:req.session,data:data1[0],data2:data2,data3:data3 });
        });
       });
      });
    });
  };

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn) =>{
              conn.query('SELECT * FROM employee as epy LEFT JOIN position as pst ON epy.employeeposition_id=pst.idposition LEFT JOIN branch as brn ON epy.employeebranch_id = brn.idbranch WHERE idemployee = ?',[id],(err,data1) =>{
               conn.query('SELECT * FROM position;',(err,data2)=>{
                conn.query('SELECT * FROM branch;',(err,data3)=>{
                if(err){
                  res.json(err);
                }
                res.render('employee/employeeUpdate',{ session:req.session,data:data1[0],data2:data2,data3:data3 });
                });
               });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn) => {
                conn.query('UPDATE employee SET ?  WHERE idemployee = ?',[data,id],(err,data1) => {
                  if(err){
                      res.json(err);
                  }else{
                    res.redirect('/employee');
                  }
               });
             });
            }
          }

module.exports = controller;
