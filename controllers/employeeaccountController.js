const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if (req.session.admin){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
      conn.query('SELECT * FROM employeeaccount ; ',(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('employeeaccount/employeeaccountList',{session:req.session,data:data1,data1:data});
      });
     });
    });
  }else {
    res.redirect('/');
  };
};

controller.add = (req,res) => {
  if (req.session.admin){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
    conn.query('SELECT * FROM type;', (err,data) => {
    res.render('employeeaccount/employeeaccountAdd',{session: req.session,data:data,data1:data1});
     });
   });
 });
}else {
  res.redirect('/');
};
};

controller.save = (req,res) => {
  if (req.session.admin){
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/employeeaccount/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                conn.query('INSERT INTO employeeaccount set ?',[data],(err,data)=>{
                    res.redirect('/employeeaccount');
                });
            });
         }
       }else {
         res.redirect('/');
       };
       };

controller.del = (req,res) => {
  if (req.session.admin){
    const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query('SELECT * FROM employeeaccount WHERE idemployeeaccount = ?',[id],(err,data1)=>{
            if(err){
                res.json(err);
            }
            res.render('employeeaccount/employeeaccountDelete',{session: req.session,data:data1[0],data1:data});
         });
        });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
  if (req.session.admin){
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM employeeaccount WHERE idemployeeaccount = ?',[id],(err,data1)=>{
              res.redirect('/employeeaccount');
          });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.edit = (req,res) => {
  if (req.session.admin){
  const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
      conn.query('SELECT * FROM employeeaccount WHERE idemployeeaccount = ?',[id],(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('employeeaccount/employeeaccountUpdate',{
          session:req.session,data:data1[0],data1:data
         });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req,res) => {
  if (req.session.admin){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
              req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM employeeaccount WHERE idemployeeaccount= ?',[id],(err,data1)=>{
                            res.render('employeeaccount/employeeaccountUpdate',{
                            session:req.session,data:data1[0]});
                        });
                     });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn) => {
                conn.query('UPDATE employeeaccount SET ?  WHERE idemployeeaccount = ?',[data,id],(err,data1) => {
                  if(err){
                      res.json(err);
                  }else{
                    res.redirect('/employeeaccount');
                  }
               });
             });
            }
          }else {
            res.redirect('/');
          };
          }



module.exports = controller;
