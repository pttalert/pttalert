
const { check } = require('express-validator');


                         exports.addValidator = [
                           check('employeeName',"Name ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeNickname',"Nickname ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeMobile',"Mobile ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeemail',"email ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeAddress',"ddress ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeposition_id',"position ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeebranch_id',"branch ไม่ถูกต้อง!").not().isEmpty()
                                                 ];

                         exports.updateValidator = [
                           check('employeeName',"Name ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeNickname',"Nickname ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeMobile',"Mobile ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeemail',"email ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeAddress',"ddress ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeeposition_id',"position ไม่ถูกต้อง!").not().isEmpty(),
                           check('employeebranch_id',"branch ไม่ถูกต้อง!").not().isEmpty()];
