const controller ={};

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query(' SELECT * from act_company',(err,act_company) =>{
          if(err){
            res.json(err);
          }
          res.render('act_company/act_companyList',{
            session:req.session,data1:data1,act_company
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};


controller.add = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
          if(err){
            res.json(err);
          }
          res.render('act_company/act_companyAdd',{
            session:req.session,data1:data1
          });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req, res) => {
  if (req.session.admin || req.session.user){
  var data = req.body;
    req.getConnection((err,conn) =>{
      conn.query('insert into act_company set ?;',[data], (err,act_company) => {
          if(err){
            res.json(err);
          }
        res.redirect('/act_company');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.edit = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from act_company where id = ?',[url.acid],(err,act_company)=>{
          if(err){
            res.json(err);
          }
          res.render('act_company/act_companyUpdate',{
            data1:data1,act_company,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
  var data = req.body;
    req.getConnection((err,conn) =>{
        conn.query('update act_company set ? where id = ?',[data,url.acid],(err,act_company)=>{
          if(err){
            res.json(err);
          }
          res.redirect('/act_company');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.confirmdelete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from act_company where id = ?',[url.acid],(err,act_company)=>{
          if(err){
            res.json(err);
          }
          res.render('act_company/act_companyDelete',{
            data1:data1,act_company,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('delete FROM act_company where id = ?;',[url.acid], (err,act_company) => {
          if(err){
            res.json(err);
          }
          res.redirect('/act_company');
      });
    });
  }else {
    res.redirect('/');
  };
};

module.exports = controller;
