const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

controller.list = (req, res) => {
  if (req.session.admin || req.session.user) {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select car.*, namecar, max(concat(date_format(datetax,"%d/%m/"),date_format(datetax,"%Y")+543)) as datetax from car join typecar on type_car = typecar.idtypecar left join tax on tax.car_id = car.id group by car.id order by car.id asc', (err, car) => {
          if (err) {
            res.json(err);
          }
          res.render('car/carList', {
            data1: data1,car,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.add = (req, res) => {
  if (req.session.admin || req.session.user) {
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query(' SELECT * from typecar', (err, typecar) => {
          if (err) {
            res.json(err);
          }
          res.render('car/carAdd', {
            data1: data1,typecar,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.save = (req, res) => {
  if (req.session.admin || req.session.user) {
    var data = req.body;
    req.getConnection((err, conn) => {
      if(req.files){
        var file = req.files.photo;
        var type = file.name.split(".");
        var filename = uuidv4()+"."+type[type.length-1];
        data.photo = filename;
        file.mv("./public/car/"+filename,function(err){
          if(err){ console.log(err) }
        });
        conn.query('insert into car set ?',[data],(err, car) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/car');
        });
      }else {
        conn.query('insert into car set ?',[data],(err, car) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/car');
        });
      }
    });
  } else {
    res.redirect('/');
  };
};

controller.detail = (req, res) => {
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('SELECT *,concat(DATE_FORMAT(datecar,"%d/%m/"),DATE_FORMAT(datecar,"%Y")+543) as date_car from car join typecar on type_car = typecar.idtypecar where id = ?', [url.caid], (err, car) => {
          conn.query('SELECT id,car_id,price,concat(DATE_FORMAT(date, "%d/%m/"),DATE_FORMAT(date,"%Y")+543) as date,concat(DATE_FORMAT(datetax, "%d/%m/"),DATE_FORMAT(datetax,"%Y")+543) as datetax from tax where car_id = ? order by date_format(date,"%Y") desc limit 1', [url.caid], (err, tax) => {
            conn.query('select * from insurance_type',(err,insurance_type)=>{
              conn.query('select * from businessinsu',(err,businessinsu)=>{
                conn.query('SELECT insurance.id,car_id,insurance_type,price,businessinsu_id,agent,concat(DATE_FORMAT(date_begin,"%d/%m/"),DATE_FORMAT(date_begin,"%Y")+543) as date_begin,concat(DATE_FORMAT(date_end,"%d/%m/"),DATE_FORMAT(date_end,"%Y")+543) as date_end, insurance_type.name as insurance_name, businessinsu.name as businessinsu from insurance join insurance_type on insurance_type = insurance_type.id join businessinsu on businessinsu_id = businessinsu.id where car_id = ? order by date_format(date_begin,"%Y") desc limit 1', [url.caid], (err, insurance) => {
                  conn.query('select * from act_company',(err,act_company)=>{
                    conn.query('SELECT act.id,buyers_act,premium,act_company_id,car_id,concat(DATE_FORMAT(start_date, "%d/%m/"),DATE_FORMAT(start_date, "%Y")+543) as start_date,concat(DATE_FORMAT(exp_date, "%d/%m/"),DATE_FORMAT(exp_date, "%Y")+543) as exp_date,act_company.name as act_company from act join act_company on act_company_id = act_company.id where car_id = ? order by date_format(start_date,"%Y") desc limit 1', [url.caid], (err, act) => {
                      if (err) {
                        res.json(err);
                      }
                      res.render('car/carDetail', {
                        data1: data1,car,tax,insurance_type,businessinsu,insurance,act,act_company,session: req.session
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.edit = (req, res) => {
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query(' SELECT * from typecar', (err, typecar) => {
          conn.query(' SELECT * from car where id = ?',[url.caid],(err, car) => {
            if (err) {
              res.json(err);
            }
            res.render('car/carUpdate', {
              data1: data1,typecar,car,session: req.session
            });
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user) {
    var url = req.params;
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('select * from car where id = ?',[url.caid],(err,photoc)=>{
        if(req.files){
          var file = req.files.photo;
          var type = file.name.split(".");
          var filename = uuidv4()+"."+type[type.length-1];
          data.photo = filename;
          file.mv("./public/car/"+filename,function(err){
            if(err){ console.log(err) }
          });
          if (photoc[0].photo) {
            fs.unlink("./public/car/" + photoc[0].photo, function (err) {
              if (err) {
                console.log(err);
              }
            });
          }
          conn.query('update car set ? where id = ?',[data,url.caid],(err, car) => {
            if (err) {
              res.json(err);
            }
            res.redirect('/car');
          });
        }else {
          if (photoc[0].photo) {
            if (photoc[0].photo != data.photo) {
              data.photo = '';
              fs.unlink("./public/car/" + photoc[0].photo, function (err) {
                if (err) {
                  console.log(err);
                }
              });
            }
          }
          conn.query('update car set ? where id = ?',[data,url.caid],(err, car) => {
            if (err) {
              res.json(err);
            }
            res.redirect('/car');
          });
        }
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.confirmdelete = (req, res) => {
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query(' SELECT * from typecar', (err, typecar) => {
          conn.query(' SELECT * from car where id = ?',[url.caid],(err, car) => {
            if (err) {
              res.json(err);
            }
            res.render('car/carDelete', {
              data1: data1,typecar,car,session: req.session
            });
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('select * from car where id = ?',[url.caid],(err,photoc)=>{
        if (photoc[0].photo) {
          fs.unlink("./public/car/"+photoc[0].photo, function (err) {
            if (err) {
              console.log(err);
            }
          });
        }
        conn.query('delete from car where id = ?',[url.caid],(err, car) => {
          if (err) {
            res.redirect('/car/confirmdelete/'+url.caid);
          }else {
            res.redirect('/car');
          }
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

module.exports = controller;
