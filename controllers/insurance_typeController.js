const controller ={};

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query(' SELECT * from insurance_type',(err,insurance_type) =>{
          if(err){
            res.json(err);
          }
          res.render('insurance_type/insurance_typeList',{
            session:req.session,data1:data1,insurance_type
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};


controller.add = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
          if(err){
            res.json(err);
          }
          res.render('insurance_type/insurance_typeAdd',{
            session:req.session,data1:data1
          });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req, res) => {
  if (req.session.admin || req.session.user){
  var data = req.body;
    req.getConnection((err,conn) =>{
      conn.query('insert into insurance_type set ?;',[data], (err,insurance_type) => {
          if(err){
            res.json(err);
          }
        res.redirect('/insurance_type');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.edit = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from insurance_type where id = ?',[url.acid],(err,insurance_type)=>{
          if(err){
            res.json(err);
          }
          res.render('insurance_type/insurance_typeUpdate',{
            data1:data1,insurance_type,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
  var data = req.body;
    req.getConnection((err,conn) =>{
        conn.query('update insurance_type set ? where id = ?',[data,url.acid],(err,insurance_type)=>{
          if(err){
            res.json(err);
          }
          res.redirect('/insurance_type');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.confirmdelete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from insurance_type where id = ?',[url.acid],(err,insurance_type)=>{
          if(err){
            res.json(err);
          }
          res.render('insurance_type/insurance_typeDelete',{
            data1:data1,insurance_type,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('delete FROM insurance_type where id = ?;',[url.acid], (err,insurance_type) => {
          if(err){
            res.json(err);
          }
          res.redirect('/insurance_type');
      });
    });
  }else {
    res.redirect('/');
  };
};

module.exports = controller;
