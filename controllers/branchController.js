const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if (req.session.admin ){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query('SELECT * FROM branch ',(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('branch/branchList',{session:req.session,data:data1,data1:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req,res) => {
  if (req.session.admin ){
    const data=req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/branch/add');
    }else{
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('INSERT INTO branch set ?',[data],(err,data1)=>{
          res.redirect('/branch');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.del = (req,res) => {
  if (req.session.admin ){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query('SELECT * FROM branch WHERE idbranch = ?',[id],(err,data1)=>{
          if(err){
            res.json(err);
          }
          res.render('branch/branchDelete',{session: req.session,data:data1[0],data1:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
  if (req.session.admin ){
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('DELETE FROM branch WHERE idbranch = ?',[id],(err,data1)=>{
          res.redirect('/branch');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.edit = (req,res) => {
  if (req.session.admin ){
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query('SELECT * FROM branch WHERE idbranch = ?',[id],(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('branch/branchUpdate',{
            session:req.session,data:data1[0],data1:data
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req,res) => {
  if (req.session.admin ){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM branch WHERE idbranch= ?',[id],(err,data1)=>{
          res.render('branch/branchUpdate',{
            session:req.session,data:data1[0]});
          });
        });
      }else{
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
        req.getConnection((err,conn) => {
          conn.query('UPDATE branch SET ?  WHERE idbranch = ?',[data,id],(err,data1) => {
            if(err){
              res.json(err);
            }else{
              res.redirect('/branch');
            }
          });
        });
      }
    }else {
      res.redirect('/');
    };
  }

  controller.add = (req,res) => {
    if (req.session.admin ){
      const data = null;
      req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM typedocument;', (err,data) => {
          res.render('branch/branchAdd',{
            session: req.session,data:data,data1:data
          });
        });
      });
    }else {
      res.redirect('/');
    };
  };

  module.exports = controller;
