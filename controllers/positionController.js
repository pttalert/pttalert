const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM position ',(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('position/positionList',{session:req.session,data:data1});
      });
    });
};

controller.save = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/position/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                conn.query('INSERT INTO position set ?',[data],(err,data1)=>{
                    res.redirect('/position');
                });
            });
         }
       };

controller.del = (req,res) => {
    const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM position WHERE idposition = ?',[id],(err,data1)=>{
            if(err){
                res.json(err);
            }
            res.render('position/positionDelete',{session: req.session,data:data1[0]});
        });
    });
};

controller.delete = (req,res) => {
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM position WHERE idposition = ?',[id],(err,data1)=>{
              res.redirect('/position');
          });
      });
    }
};

controller.edit = (req,res) => {
  const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM position WHERE idposition = ?',[id],(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('position/positionUpdate',{
          session:req.session,data:data1[0]
        });
      });
    });
};

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
              req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM position WHERE idposition= ?',[id],(err,data1)=>{
                            res.render('position/positionUpdate',{
                            session:req.session,data:data1[0]});
                        });
                     });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn) => {
                conn.query('UPDATE position SET ?  WHERE idposition = ?',[data,id],(err,data1) => {
                  if(err){
                      res.json(err);
                  }else{
                    res.redirect('/position');
                  }
               });
             });
            }
          }

controller.add = (req,res) => {
    const data = null;
    res.render('position/positionAdd',{
    session: req.session,data:data
    });
};

module.exports = controller;
