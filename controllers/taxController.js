const controller = {};

controller.list = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select id,car_id,price,concat(DATE_FORMAT(date,"%d/%m/"),DATE_FORMAT(date,"%Y")+543) as date,concat(DATE_FORMAT(datetax,"%d/%m/"),DATE_FORMAT(datetax,"%Y")+543) as datetax from tax where car_id = ?',[url.caid],(err,tax)=>{
          res.render('tax/taxList',{
            data1,tax,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.save = (req, res) => {
  if (req.session.admin || req.session.user) {
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('insert into tax set ?',[data],(err, tax) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/cardetail/'+data.car_id);
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('update tax set ? where id = ?',[data,url.taid],(err, tax) => {
        if (err) {
          res.json(err);
        }
        if (c[2] == "page") {
          res.redirect('/tax/list/'+url.caid);
        }else {
          res.redirect('/cardetail/'+url.caid);
        }
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('delete from tax where id = ?',[url.taid],(err, tax) => {
        if (err) {
          res.json(err);
        }
        if (c[2] == "page") {
          res.redirect('/tax/list/'+url.caid);
        }else {
          res.redirect('/cardetail/'+url.caid);
        }
      });
    });
  } else {
    res.redirect('/');
  };
};


controller.edit = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select * from tax where id = ?',[url.taid],(err,tax)=>{
          res.render('tax/taxUpdate',{
            data1,tax,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.confirmdelete = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select id,car_id,price,concat(DATE_FORMAT(date,"%d/%m/"),DATE_FORMAT(date,"%Y")+543) as date,concat(DATE_FORMAT(datetax,"%d/%m/"),DATE_FORMAT(datetax,"%Y")+543) as datetax from tax where id = ?',[url.taid],(err,tax)=>{
          res.render('tax/taxDelete',{
            data1,tax,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

module.exports = controller;
