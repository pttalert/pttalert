const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if (req.session.admin ){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ',(err,data1) =>{
        conn.query('SELECT * FROM user ;', (err,data) => {
          if(err){
            res.json(err);
          }
          res.render('user/userList',{session:req.session,data1:data1,data:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.add = (req,res) => {
  if (req.session.admin ){
    const data = null;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        res.render('user/userAdd',{session: req.session,data1:data1});
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req,res) => {
  if (req.session.admin ){
    const data = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/user/add');
    }else{
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('INSERT INTO user set ?',[data],(err,data)=>{
          res.redirect('/user');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.del = (req,res) => {
  if (req.session.admin ){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
      conn.query('SELECT * FROM typedocument ;', (err,data1) => {
        conn.query('SELECT * FROM user WHERE iduser = ?',[id],(err,data)=>{
          res.render('user/userDelete',{session:req.session ,data:data[0],data1:data1});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
  if (req.session.admin ){
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('DELETE FROM user WHERE iduser = ?',[id],(err,data)=>{
          res.redirect('/user');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.edit = (req,res) => {
  if (req.session.admin ){
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ;', (err,data) => {
        conn.query('SELECT * FROM user WHERE iduser = ?',[id],(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('user/userUpdate',{session:req.session,data:data1[0],data1:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req,res) => {
  if (req.session.admin ){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM user WHERE iduser = ?',[id],(err,data1)=>{
          res.render('user/userUpdate',{session:req.session,data:data1[0]});
        });
      });
    }else{
      req.session.success=true;
      req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn) => {
        conn.query('UPDATE user SET ?  WHERE iduser = ?',[data,id],(err,data1) => {
          if(err){
            res.json(err);
          }else{
            res.redirect('/user');
          }
        });
      });
    }
  }else {
    res.redirect('/');
  };
}



module.exports = controller;
