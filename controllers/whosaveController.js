const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM historywho ',(err,data) =>{
        conn.query('SELECT * FROM typedocument ;', (err,data1) => {
          if(err){
            res.json(err);
          }
          res.render('whoSavehistory/whoList',{session:req.session,data:data,data1:data1});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.add = (req,res) => {
  if (req.session.admin || req.session.user){
    const data = null;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        res.render('whoSavehistory/whoAdd',{session: req.session,data1:data1});
      });
    });
  }else {
    res.redirect('/');
  };
};


controller.save = (req,res) => {
  if (req.session.admin || req.session.user){
    const data = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/whosave/add');
    }else{
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('INSERT INTO historywho set ?',[data],(err,data1)=>{
          res.redirect('/whosave');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.del = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    req.getConnection((err,conn)=>{
      conn.query('SELECT * FROM typedocument ;', (err,data1) => {
        conn.query('SELECT * FROM historywho WHERE id = ?',[id],(err,data)=>{
          res.render('whoSavehistory/whoDelete',{session:req.session ,data:data[0],data1:data1});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('DELETE FROM historywho WHERE id = ?',[id],(err,data)=>{
          res.redirect('/whosave');
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.edit = (req,res) => {
  if (req.session.admin || req.session.user){
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ;', (err,data1) => {
        conn.query('SELECT * FROM historywho WHERE id = ?',[id],(err,data) =>{
          if(err){
            res.json(err);
          }
          res.render('whoSavehistory/whoUpdate',{session:req.session,data:data[0],data1:data1});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    const data = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM typedocument ;', (err,data1) => {
          conn.query('SELECT * FROM historywho WHERE id = ?',[id],(err,data)=>{
            res.render('whoSavehistory/whoUpdate',{session:req.session,data:data[0],data1:data1});
          });
        });
      });
    }else{
      req.session.success=true;
      req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn) => {
        conn.query('UPDATE historywho SET ?  WHERE id = ?',[data,id],(err,data) => {
          if(err){
            res.json(err);
          }else{
            res.redirect('/whosave');
          }
        });
      });
    }
  }else {
    res.redirect('/');
  };
}

controller.addadd = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        res.render('whoSavehistory/whoAddadd',{session: req.session,data1:data1,id:id});
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.saveadd = (req,res) => {
  if (req.session.admin || req.session.user){
    const data = req.body;
    const {id} = req.params;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/whosave/add/add/'+id);
    }else{
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
        conn.query('INSERT INTO historywho set ?',[data],(err,data1)=>{
          res.redirect('/dochistory/dochistoryAdd/'+id);
        });
      });
    }
  }else {
    res.redirect('/');
  };
};

module.exports = controller;
