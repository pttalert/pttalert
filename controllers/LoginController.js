const controller = {};
const { validationResult } = require('express-validator');

controller.loginForm = (req, res) => {
    res.render("Login", { session:req.session });
};

controller.login = (req, res) => {
    const data1 = req.body;
    const data2 = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
        const username = req.body.username;
        const password = req.body.password;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM user where username = ? AND password = ?', [username, password], (err, data1) => {
              conn.query('SELECT * FROM employeeaccount where username = ? AND password = ?', [username, password], (err, data2) => {
                console.log(data1);
                console.log(data2);
                    if (data1.length > 0) {
                        req.session.success = true;
                        req.session.topic = "USERLOGIN สำเร็จ";
                        req.session.user = data1[0].name;
                        req.session.userid = data1[0].iduser;
                        res.redirect('/home');
                    } else if(data2.length>0 ){
                      req.session.success = true;
                      req.session.topic = "ADMINLOGIN สำเร็จ";
                      req.session.admin = data2[0].employeeaccountName;
                      req.session.adminid = data2[0].idemployeeaccount;
                      res.redirect('/home');
                    }else {
                      req.session.success = true;
                      req.session.topic = "Username หรือ Password ไม่ถูกต้อง!!";
                      res.redirect('/');
                    }
                })
            })
        })
    };
};

controller.logoutt = function (req, res) {
    req.session.destroy();
    res.redirect('/');
};

controller.logintest1 = (req, res) => {
    res.render("test/test1", { session:req.session });
};
module.exports = controller;
