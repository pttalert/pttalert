
const { check } = require('express-validator');


                         exports.addValidator = [
                                                 check('documentNumber',"กรุณากรอกเลขที่เอกสาร").not().isEmpty(),
                                                 check('documentName',"กรุณากรอกชื่อเอกสาร!").not().isEmpty(),
                                                 check('documentRenewaldate',"กรุณากรอกวันที่ต่ออายุ!").not().isEmpty(),
                                                 check('documentExpirationdate',"กรุณากรอกวันที่หมดอาย!").not().isEmpty(),
                                                 check('documentContactagency',"กรุณากรอกสถานที่ติดต่อ!").not().isEmpty(),
                                                 check('detial',"กรุณากรอกรายละเอียด!").not().isEmpty(),
                                                 check('typedocument_id',"กรุณาระบุประเภท!").not().isEmpty(),
                                                 // check('billing_id',"กรุณาระบุประจำรอบ!").not().isEmpty(),
                                                 // check('branch_id',"กรุณาระบุสาขา!").not().isEmpty()
                                                 ];

                                                 // exports.savehistory = [
                                                 //   check('Name',"กรุณาเลือกชื่อ").not().isEmpty()
                                                 // ];

                                                 exports.UpdateValidator = [
                                                   check('documentNumber',"กรุณากรอกเลขที่เอกสาร").not().isEmpty(),
                                                   check('documentName',"กรุณากรอกชื่อเอกสาร!").not().isEmpty(),
                                                   check('documentRenewaldate',"กรุณากรอกวันที่ต่ออายุ!").not().isEmpty(),
                                                   check('documentExpirationdate',"กรุณากรอกวันที่หมดอาย!").not().isEmpty(),
                                                   check('documentContactagency',"กรุณากรอกสถานที่ติดต่อ!").not().isEmpty(),
                                                   check('detial',"กรุณากรอกรายละเอียด!").not().isEmpty(),
                                                   check('typedocument_id',"กรุณาระบุประเภท!").not().isEmpty(),
                                                   check('billing_id',"กรุณาระบุประจำรอบ!").not().isEmpty(),
                                                   check('branch_id',"กรุณาระบุสาขา!").not().isEmpty()
                                                 ]
