const controller = {};

controller.list = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select act.id,buyers_act,premium,act_company_id,car_id,concat(DATE_FORMAT(start_date, "%d/%m/"),DATE_FORMAT(start_date,"%Y")+543) as start_date,concat(DATE_FORMAT(exp_date, "%d/%m/"),DATE_FORMAT(exp_date,"%Y")+543) as exp_date,act_company.name as act_company from act join act_company on act_company_id = act_company.id where car_id = ?',[url.caid],(err,act)=>{
          res.render('act/actList',{
            data1,act,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.save = (req, res) => {
  if (req.session.admin || req.session.user) {
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('insert into act set ?',[data],(err, act) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/cardetail/'+data.car_id);
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    var data = req.body;
    req.getConnection((err, conn) => {
        conn.query('update act set ? where id = ?',[data,url.acid],(err, act) => {
          if (err) {
            res.json(err);
          }
          if (c[2] == "page") {
            res.redirect('/act/list/'+url.caid);
          }else {
            res.redirect('/cardetail/'+url.caid);
          }
        });
    });
  } else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    req.getConnection((err, conn) => {
        conn.query('delete from act where id = ?',[url.acid],(err, act) => {
          if (err) {
            res.json(err);
          }
          if (c[2] == "page") {
            res.redirect('/act/list/'+url.caid);
          }else {
            res.redirect('/cardetail/'+url.caid);
          }
        });
    });
  } else {
    res.redirect('/');
  };
};

controller.edit = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select * from act_company',(err,act_company)=>{
          conn.query('select act.*,act_company.name as act_company from act join act_company on act_company_id = act_company.id where act.id = ?',[url.acid],(err,act)=>{
            res.render('act/actUpdate',{
              data1,act_company,act,url,session: req.session
            });
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.confirmdelete = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select act.id,buyers_act,premium,act_company_id,car_id,concat(DATE_FORMAT(start_date, "%d/%m/"),DATE_FORMAT(start_date,"%Y")+543) as start_date,concat(DATE_FORMAT(exp_date, "%d/%m/"),DATE_FORMAT(exp_date,"%Y")+543) as exp_date,act_company.name as act_company from act join act_company on act_company_id = act_company.id where act.id = ?',[url.acid],(err,act)=>{
          res.render('act/actDelete',{
            data1,act,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

module.exports = controller;
