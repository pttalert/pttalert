const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ',(err,data1) =>{
        conn.query('SELECT * FROM typedocument ;', (err,data) => {
        if(err){
          res.json(err);
        }
        res.render('typedocument/typedocumentList',{session:req.session,data:data1,data1:data});
      });
    });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req,res) => {
  if (req.session.admin || req.session.user){
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/typedocument/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn)=>{
                conn.query('INSERT INTO typedocument set ?',[data],(err,data1)=>{
                    res.redirect('/typedocument');
                });
            });
         }
       }else {
         res.redirect('/');
       };
       };

controller.del = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM typedocument ;', (err,data) => {
        conn.query('SELECT * FROM typedocument WHERE idtypedocument = ?',[id],(err,data1)=>{
            res.render('typedocument/typedocumentDelete',{
              session:req.session ,data:data1[0],data1:data
             });
           });
        });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
    } else {
      req.session.success = true;
      req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      req.getConnection((err,conn)=>{
          conn.query('DELETE FROM typedocument WHERE idtypedocument = ?',[id],(err,data1)=>{
              res.redirect('/typedocument');
          });
      });
    }
  }else {
    res.redirect('/');
  };
};

controller.edit = (req,res) => {
  if (req.session.admin || req.session.user){
  const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ;', (err,data) => {
      conn.query('SELECT * FROM typedocument WHERE idtypedocument = ?',[id],(err,data1) =>{
        if(err){
          res.json(err);
        }
        res.render('typedocument/typedocumentUpdate',{
          session:req.session,data:data1[0],data1:data
        });
      });
    });
  });
}else {
  res.redirect('/');
};
};

controller.update = (req,res) => {
  if (req.session.admin || req.session.user){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
              req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM typedocument WHERE idtypedocument= ?',[id],(err,data1)=>{
                            res.render('typedocument/typedocumentUpdate',{
                            session:req.session,data:data1[0]});
                        });
                     });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
              req.getConnection((err,conn) => {
                conn.query('UPDATE typedocument SET ?  WHERE idtypedocument = ?',[data,id],(err,data1) => {
                  if(err){
                      res.json(err);
                  }else{
                    res.redirect('/typedocument');
                  }
               });
             });
            }
          }else {
            res.redirect('/');
          };
          }

controller.add = (req,res) => {
  if (req.session.admin || req.session.user){
    const data = null;
    req.getConnection((err,conn) => {
    conn.query('SELECT * FROM typedocument;', (err,data) => {
    res.render('typedocument/typedocumentAdd',{
    session: req.session,data1:data
     });
    });
    });
  }else {
    res.redirect('/');
  };
};

module.exports = controller;
