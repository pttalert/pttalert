const controller ={};

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query(' SELECT * from businessinsu',(err,businessinsu) =>{
          if(err){
            res.json(err);
          }
          res.render('businessinsu/businessinsuList',{
            session:req.session,data1:data1,businessinsu
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};


controller.add = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
          if(err){
            res.json(err);
          }
          res.render('businessinsu/businessinsuAdd',{
            session:req.session,data1:data1
          });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req, res) => {
  if (req.session.admin || req.session.user){
  var data = req.body;
    req.getConnection((err,conn) =>{
      conn.query('insert into businessinsu set ?;',[data], (err,businessinsu) => {
          if(err){
            res.json(err);
          }
        res.redirect('/businessinsu');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.edit = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from businessinsu where id = ?',[url.acid],(err,businessinsu)=>{
          if(err){
            res.json(err);
          }
          res.render('businessinsu/businessinsuUpdate',{
            data1:data1,businessinsu,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
  var data = req.body;
    req.getConnection((err,conn) =>{
        conn.query('update businessinsu set ? where id = ?',[data,url.acid],(err,businessinsu)=>{
          if(err){
            res.json(err);
          }
          res.redirect('/businessinsu');
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.confirmdelete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data1) => {
        conn.query('select * from businessinsu where id = ?',[url.acid],(err,businessinsu)=>{
          if(err){
            res.json(err);
          }
          res.render('businessinsu/businessinsuDelete',{
            data1:data1,businessinsu,session:req.session
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user){
  var url = req.params;
    req.getConnection((err,conn) =>{
      conn.query('delete FROM businessinsu where id = ?;',[url.acid], (err,businessinsu) => {
          if(err){
            res.json(err);
          }
          res.redirect('/businessinsu');
      });
    });
  }else {
    res.redirect('/');
  };
};

module.exports = controller;
