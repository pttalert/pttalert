const controller = {};


controller.list = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select insurance.id,car_id,insurance_type,price,businessinsu_id,agent,concat(DATE_FORMAT(date_begin,"%d/%m/"),DATE_FORMAT(date_begin,"%Y")+543) as date_begin,concat(DATE_FORMAT(date_end,"%d/%m/"),DATE_FORMAT(date_end,"%Y")+543) as date_end,insurance_type.name as insurance_name,businessinsu.name as businessinsu from insurance join insurance_type on insurance_type = insurance_type.id join businessinsu on businessinsu_id = businessinsu.id where car_id = ?',[url.caid],(err,insurance)=>{
          res.render('insurance/insuranceList',{
            data1,insurance,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.save = (req, res) => {
  if (req.session.admin || req.session.user) {
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('insert into insurance set ?',[data],(err, insurance) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/cardetail/'+data.car_id);
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.update = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    var data = req.body;
    req.getConnection((err, conn) => {
      conn.query('update insurance set ? where id = ?',[data,url.inid],(err, insurance) => {
        if (err) {
          res.json(err);
        }
        if (c[2] == "page") {
          res.redirect('/insurance/list/'+url.caid);
        }else {
          res.redirect('/cardetail/'+url.caid);
        }
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.delete = (req, res) => {
  if (req.session.admin || req.session.user) {
    var origin = req.originalUrl;
    var c = origin.split('/');
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('delete from insurance where id = ?',[url.inid],(err, insurance) => {
        if (err) {
          res.json(err);
        }
        if (c[2] == "page") {
          res.redirect('/insurance/list/'+url.caid);
        }else {
          res.redirect('/cardetail/'+url.caid);
        }
      });
    });
  } else {
    res.redirect('/');
  };
};

controller.edit = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select * from insurance_type',(err,insurance_type)=>{
          conn.query('select * from businessinsu',(err,businessinsu)=>{
            conn.query('select insurance.*,insurance_type.name as insurance_name,businessinsu.name as businessinsu from insurance join insurance_type on insurance_type = insurance_type.id join businessinsu on businessinsu_id = businessinsu.id where insurance.id = ?',[url.inid],(err,insurance)=>{
              res.render('insurance/insuranceUpdate',{
                data1,insurance_type,businessinsu,insurance,url,session: req.session
              });
            });
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

controller.confirmdelete = (req,res)=>{
  if (req.session.admin || req.session.user) {
    var url = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err, data1) => {
        conn.query('select insurance.id,car_id,insurance_type,price,businessinsu_id,agent,concat(DATE_FORMAT(date_begin,"%d/%m/"),DATE_FORMAT(date_begin,"%Y")+543) as date_begin,concat(DATE_FORMAT(date_end,"%d/%m/"),DATE_FORMAT(date_end,"%Y")+543) as date_end,insurance_type.name as insurance_name,businessinsu.name as businessinsu from insurance join insurance_type on insurance_type = insurance_type.id join businessinsu on businessinsu_id = businessinsu.id where insurance.id = ?',[url.inid],(err,insurance)=>{
          res.render('insurance/insuranceDelete',{
            data1,insurance,url,session: req.session
          });
        });
      });
    });
  } else {
    res.redirect('/');
  };
}

module.exports = controller;
