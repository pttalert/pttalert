const controller ={};
const { validationResult } = require('express-validator');
const uuidv4 = require('uuid').v4;

controller.list = (req, res) => {
  if (req.session.admin || req.session.user){
    var data2 = [{typedocumentName:"เอกสาร"}] ;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query(' SELECT  dcm.iddocument,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(DATE_ADD(documentExpirationdate, INTERVAL 543 YEAR)," %d/%m/%Y")ddd FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE select_document = 0 ORDER BY documentExpirationdate ASC ; ',(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('document/documentList',{session:req.session,data:data1,data1:data,data2:data2});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.type = (req, res) => {
  if (req.session.admin || req.session.user){
    const { id } = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument ;', (err,data) => {
        conn.query('SELECT * FROM typedocument where idtypedocument = ? ;',[id],(err,data2) => {
          conn.query(' SELECT dcm.iddocument,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(DATE_ADD(documentExpirationdate, INTERVAL 543 YEAR)," %d/%m/%Y")ddd FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE select_document = 0 and idtypedocument = ? ORDER BY documentExpirationdate ASC ; ',[id],(err,data1) =>{
            if(err){
              res.json(err);
            }
            res.render('document/documentList',{session:req.session,data:data1,data1:data,data2:data2});
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.listhistory = (req, res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query(' SELECT dcm.iddocument,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(documentExpirationdate," %d/%m/%Y")ddd FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE select_document = 1 ORDER BY documentExpirationdate ASC ; ',(err,data1) =>{
          if(err){
            res.json(err);
          }
          res.render('document/documentListhistory',{session:req.session,data:data1,data1:data});
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.add = (req,res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM document ;',(err,doc) => {
        conn.query('SELECT * FROM typedocument;', (err,data4) => {
          conn.query('SELECT * FROM billing;', (err,data3) => {
            conn.query('SELECT * FROM branch;', (err,data2) => {
              res.render('document/documentAdd',{ session: req.session,data1:data4,data3:data3,data2:data2,doc:doc});
            });
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.addcar = (req,res) => {
  if (req.session.admin || req.session.user){
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM typedocument;', (err,data) => {
        conn.query('SELECT * FROM billing;', (err,data3) => {
          conn.query('SELECT * FROM branch;', (err,data2) => {
            conn.query('SELECT * FROM typecar;', (err,data4) => {
              res.render('document/documentAddcar',{ session: req.session,data1:data,data3:data3,data2:data2,data4:data4 });
            });
          });
        });
      });
    });
  }else {
    res.redirect('/');
  };
};

controller.save = (req,res) => {
  if (req.session.admin || req.session.user){
    const data = req.body;
    const errors = validationResult(req);
    if (data.documentContactperson == "") { data.documentContactperson = 0; }
    if (data.documentContactnumber == "") { data.documentContactnumber = 0; }
    if (data.billing_id == "") { data.billing_id = null; }
    if (data.branch_id == "") { data.branch_id = null; }
    if (data.unitmy == "") { data.unitmy = null; }
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/document/add');
    }else{
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
      if (req.files){
        var file = req.files.filename;
        //var filename = file.name;
        if(!Array.isArray(file)){
          var filesname = uuidv4()+"."+file.name.split(".")[1];
          file.mv("./public/"+filesname,function(err){
            if (err){console.log(err)}
          })
        }else {
          for(var i = 0 ; i < file.length ; i++){
            var filesname = uuidv4()+"."+file[i].name.split(".")[1];
            file[i].mv("./public/"+filesname,function(err){
              if (err){
                console.log(err)}
              })
            }
          }
        }
        console.log(filesname);
        req.getConnection((err,conn)=>{
          conn.query('INSERT INTO document set ? , documentScan = ? ',[data,filesname],(err,data1)=>{
            res.redirect('/home');
          });
        });
      }
    }else {
      res.redirect('/');
    };
  };


  controller.del = (req,res) => {
    if (req.session.admin || req.session.user){
      const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM typedocument;', (err,data1) => {
          conn.query('SELECT   dcm.unitmy as dcmunit, bll.idBilling as billid, DATE_FORMAT(DATE_ADD(Alerting, INTERVAL 543 YEAR)," %d/%m/%Y")hwtsm , brh.branchName as brhname, bll.BillingName as billname, tdm.typedocumentName tdmname, tyc.namecar as tpcn, dcm.Carnumber as dcmCarnumber, dcm.Carbrand as dcmbann, dcm.documentContactnumber as dcmber, dcm.documentContactperson as dcmwho, dcm.documentContactagency as dcmban, dcm.documentNumber as dcmnumb, dcm.documentName as dcmnam,  dcm.documentScan as dcmscan, dcm.documentScan as dcmpoto, dcm.iddocument as iddoc,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(DATE_ADD(documentExpirationdate, INTERVAL 543 YEAR)," %d/%m/%Y")ddd, DATE_FORMAT(DATE_ADD(documentRenewaldate, INTERVAL 543 YEAR)," %d/%m/%Y")bbb  FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id LEFT JOIN typecar as tyc ON tyc.idtypecar=dcm.typecar WHERE iddocument = ?',[id],(err,data)=>{
            if(err){
              res.json(err);
            }
            res.render('document/documentDelete',{session: req.session,data:data[0],data1:data1});
          });
        });
      });
    }else {
      res.redirect('/');
    };
  };

  controller.delete = (req,res) => {
    if (req.session.admin || req.session.user){
      const { id } = req.params;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
      } else {
        req.session.success=true;
        req.session.topic="ลบข้อมูลเสร็จแล้ว";
        req.getConnection((err,conn)=>{
          conn.query('DELETE FROM document WHERE iddocument = ?  ',[id],(err,data1)=>{
            res.redirect('/home');
          });
        });
      }
    }else {
      res.redirect('/');
    };
  };

  controller.delhistory = (req,res) => {
    if (req.session.admin || req.session.user){
      const { id } = req.params;
      req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM typedocument;', (err,data) => {
          conn.query('SELECT   dcm.Carnumber as dcmCarnumber, dcm.Carbrand as dcmbann, dcm.documentContactnumber as dcmber, dcm.documentContactperson as dcmwho, dcm.documentContactagency as dcmban, dcm.documentNumber as dcmnumb, dcm.documentName as dcmnam,  dcm.documentScan as dcmscan, dcm.documentScan as dcmpoto, dcm.iddocument as iddoc,brh.branchName bname,tdm.typedocumentName tdmname,dcm.documentContactperson dcmcc,dcm.documentContactnumber dcmaaa,dcm.documentName dcmname,dcm.detial cdmdetial, DATE_FORMAT(documentExpirationdate," %d/%m/%Y")ddd, DATE_FORMAT(documentRenewaldate," %d/%m/%Y")bbb FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE iddocument = ?',[id],(err,data1)=>{
            if(err){
              res.json(err);
            }
            res.render('document/documentDeletehistory',{session: req.session,data:data1[0],data1:data});
          });
        });
      });
    }else {
      res.redirect('/');
    };
  };

  controller.deletehistory = (req,res) => {
    if (req.session.admin || req.session.user){
      const { id } = req.params;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
      } else {
        req.session.success = true;
        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        req.getConnection((err,conn)=>{
          conn.query('DELETE FROM document WHERE iddocument = ?',[id],(err,data1)=>{
            res.redirect('/documenthistory');
          });
        });
      }
    }else {
      res.redirect('/');
    };
  };

  controller.edit = (req,res) => {
    if (req.session.admin || req.session.user){
      const {id} = req.params;
      req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM typecar;', (err,data4) => {
          conn.query('SELECT *  FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE iddocument = ?',[id],(err,data) =>{
            conn.query('SELECT * FROM typedocument;', (err,data1) => {
              conn.query('SELECT * FROM billing;', (err,data2) => {
                conn.query('SELECT * FROM branch;', (err,data3) => {
                  conn.query('SELECT DATE_FORMAT(documentRenewaldate, "%Y-%m-%d")dRdate1,DATE_FORMAT(documentExpirationdate, "%Y-%m-%d")dRdate2 FROM document WHERE iddocument = ?;',[id], (err,dRdate) => {
                    if(err){
                      res.json(err);
                    }
                    res.render('document/documentUpdate',{ session:req.session,data:data[0],data1:data1,data2:data2,data3:data3,dRdate:dRdate[0],data4:data4});
                  });
                });
              });
            });
          });
        });
      });
    }else {
      res.redirect('/');
    };
  };

  controller.update = (req,res) => {
    if (req.session.admin || req.session.user){
      const errors = validationResult(req);
      const { id } = req.params;
      const data = req.body;
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        req.getConnection((err,conn) =>{
          conn.query('SELECT *  FROM document as dcm LEFT JOIN typedocument as tdm ON tdm.idtypedocument=dcm.typedocument_id LEFT JOIN billing as bll ON bll.idBilling=dcm.billing_id LEFT JOIN branch as brh ON brh.idbranch=dcm.branch_id WHERE iddocument = ?',[id],(err,data) =>{
            conn.query('SELECT * FROM typedocument;', (err,data1) => {
              conn.query('SELECT * FROM billing;', (err,data2) => {
                conn.query('SELECT * FROM branch;', (err,data3) => {
                  conn.query('SELECT DATE_FORMAT(documentRenewaldate, "%Y-%m-%d")dRdate1,DATE_FORMAT(documentExpirationdate, "%Y-%m-%d")dRdate2 FROM document WHERE iddocument = ?;',[id], (err,dRdate) => {
                    if(err){
                      res.json(err);
                    }
                    res.render('document/documentUpdate',{ session:req.session,data:data[0],data1:data1,data2:data2,data3:data3,dRdate:dRdate[0] });
                  });
                });
              });
            });
          });
        });
      }else{
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
        req.getConnection((err,conn)=>{
          conn.query('UPDATE document set ? WHERE iddocument = ?',[data,id],(err,data1)=>{
            res.redirect('/document/edit/'+id);
          });
        });
      }
    }else {
      res.redirect('/');
    };
  }

  controller.updatepoto = (req,res) => {
    if (req.session.admin || req.session.user){
      const { id } = req.params;
      const errors = validationResult(req);
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        res.redirect('/document/add');
      }else{
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
        if (req.files){
          var file = req.files.filename;
          //var filename = file.name;
          if(!Array.isArray(file)){
            var filesname = uuidv4()+"."+file.name.split(".")[1];
            file.mv("./public/"+filesname,function(err){
              if (err){console.log(err)}
            })
          }else {
            for(var i = 0 ; i < file.length ; i++){
              var filesname = uuidv4()+"."+file[i].name.split(".")[1];
              file[i].mv("./public/"+filesname,function(err){
                if (err){
                  console.log(err)}
                })
              }
            }
          }
          req.getConnection((err,conn)=>{
            conn.query('UPDATE document set documentScan = ? WhERE iddocument = ? ',[filesname,id],(err,data1)=>{
              res.redirect('/document/edit/'+id);
            });
          });

        }
      }else {
        res.redirect('/');
      };
    };

    module.exports = controller;
