const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const upload = require("express-fileupload");
const app = express();
const uuidv4 = require('uuid').v4;

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(upload());
app.use(express.static('public'));
app.use('/css',express.static('css'));
app.use('/vendor',express.static('vendor'));
app.use('/js',express.static('js'));
app.use(express.static('public'));
app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(session({
    secret:'Passw0rd',
    resave: true,
    saveUninitialized: true
}));
app.use(connection(mysql, {
    host: 'database-ptt.ce9imirwsreh.ap-southeast-1.rds.amazonaws.com',
    user: 'pttalert',
    password: 'pttalertPassw0rd',
    port: 3306,
    database: 'pttalert2020',
    timezone: 'utc'
}, 'single'));

const routeLoginz = require('./routes/loginRoute');
app.use('/', routeLoginz);
const routehome = require('./routes/homeRoute');
app.use('/', routehome);
const routeuiemployeeaccount = require('./routes/uiemployeeaccountRoute');
app.use('/', routeuiemployeeaccount);
const routeuiposition = require('./routes/uipositionRoute');
app.use('/', routeuiposition);
const routeuitypedocument = require('./routes/uitypedocumentRoute');
app.use('/', routeuitypedocument);
const routeuibranch = require('./routes/uibranchRoute');
app.use('/', routeuibranch);
const routeuiemployee = require('./routes/uiemployeeRoute');
app.use('/', routeuiemployee);
const routeuidocument = require('./routes/uidocumentRoute');
app.use('/', routeuidocument);
const routeuibilling = require('./routes/uibillingRoute');
app.use('/', routeuibilling);
const routetest = require('./routes/testRoute');
app.use('/', routetest);
const routedochittory = require("./routes/uidochitory");
app.use('/',routedochittory);
const routewhosave = require("./routes/whosaveRoute");
app.use('/',routewhosave);
const uiuserRoute = require("./routes/uiuserRoute");
app.use('/',uiuserRoute);
//++++++++++++++++++++++++++++
const carRoute = require("./routes/carRoute");
app.use('/',carRoute);

const act_companyRoute = require("./routes/act_companyRoute");
app.use('/',act_companyRoute);

const taxRoute = require("./routes/taxRoute");
app.use('/',taxRoute);

const insurance_typeRoute = require("./routes/insurance_typeRoute");
app.use('/',insurance_typeRoute);

const businessinsuRoute = require("./routes/businessinsuRoute");
app.use('/',businessinsuRoute);

const insuranceRoute = require("./routes/insuranceRoute");
app.use('/',insuranceRoute);

const actRoute = require("./routes/actRoute");
app.use('/',actRoute);

app.listen('8089');
