const express = require('express');
const router = express.Router();
const whosaveController = require('../controllers/whosaveController');
const whosaveValidator = require('../controllers/whosaveValidator');

router.get('/whosave',whosaveController.list);
router.get('/whosave/add',whosaveController.add);
router.post('/whosave/save',whosaveValidator.addValidator,whosaveController.save);
router.get('/whosave/add/add/:id',whosaveController.addadd);
router.post('/whosave/saveadd/:id',whosaveValidator.addValidator,whosaveController.saveadd);
router.get('/whosave/del/:id',whosaveController.del);
router.get('/whosave/delete/:id',whosaveController.delete);
router.get('/whosave/edit/:id',whosaveController.edit);
router.post('/whosave/update/:id',whosaveController.update);

module.exports = router;
