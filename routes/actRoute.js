const express = require('express');
const router = express.Router();
const actController = require('../controllers/actController');

router.get('/act/list/:caid',actController.list);
router.post('/act/save',actController.save);
router.post('/act/update/:caid/:acid',actController.update);
router.get('/act/delete/:caid/:acid',actController.delete);

router.get('/act/page/edit/:caid/:acid',actController.edit);
router.post('/act/page/update/:caid/:acid',actController.update);
router.get('/act/page/confirmdelete/:caid/:acid',actController.confirmdelete);
router.get('/act/page/delete/:caid/:acid',actController.delete);

module.exports = router;
