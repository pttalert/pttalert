const express = require('express');
const router = express.Router();
const insurance_typeController = require('../controllers/insurance_typeController');

router.get('/insurance_type',insurance_typeController.list);
router.get('/insurance_type/add',insurance_typeController.add);
router.post('/insurance_type/save',insurance_typeController.save);
router.get('/insurance_type/edit/:acid',insurance_typeController.edit);
router.post('/insurance_type/update/:acid',insurance_typeController.update);
router.get('/insurance_type/confirmdelete/:acid',insurance_typeController.confirmdelete);
router.get('/insurance_type/delete/:acid',insurance_typeController.delete);

module.exports = router;
