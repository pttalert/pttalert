const express = require('express');
const router = express.Router();
const taxController = require('../controllers/taxController');

router.get('/tax/list/:caid',taxController.list);
router.post('/tax/save',taxController.save);
router.post('/tax/update/:caid/:taid',taxController.update);
router.get('/tax/delete/:caid/:taid',taxController.delete);

router.get('/tax/page/edit/:caid/:taid',taxController.edit);
router.post('/tax/page/update/:caid/:taid',taxController.update);
router.get('/tax/page/confirmdelete/:caid/:taid',taxController.confirmdelete);
router.get('/tax/page/delete/:caid/:taid',taxController.delete);

module.exports = router;
