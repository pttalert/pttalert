const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.get('/user',userController.list);
router.get('/user/add',userController.add);
router.post('/user/save',userController.save);
router.get('/user/del/:id',userController.del);
router.get('/user/delete/:id',userController.delete);
router.get('/user/edit/:id',userController.edit);
router.post('/user/update/:id',userController.update);

module.exports = router;
