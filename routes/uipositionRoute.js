const express = require('express');
const router = express.Router();
const positionController = require('../controllers/positionController');
const positionValidator = require('../controllers/positionValidator');

router.get('/position',positionController.list);
router.get('/position/add',positionController.add);
router.post('/position/save',positionValidator.addValidator,positionController.save);
router.get('/position/del/:id',positionController.del);
router.get('/position/delete/:id',positionController.delete);
router.get('/position/edit/:id',positionController.edit);
router.post('/position/update/:id',positionValidator.updateValidator,positionController.update);

module.exports = router;
