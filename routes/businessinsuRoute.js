const express = require('express');
const router = express.Router();
const businessinsuController = require('../controllers/businessinsuController');

router.get('/businessinsu',businessinsuController.list);
router.get('/businessinsu/add',businessinsuController.add);
router.post('/businessinsu/save',businessinsuController.save);
router.get('/businessinsu/edit/:acid',businessinsuController.edit);
router.post('/businessinsu/update/:acid',businessinsuController.update);
router.get('/businessinsu/confirmdelete/:acid',businessinsuController.confirmdelete);
router.get('/businessinsu/delete/:acid',businessinsuController.delete);

module.exports = router;
