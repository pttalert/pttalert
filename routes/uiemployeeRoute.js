const express = require('express');
const router = express.Router();
const employeeController = require('../controllers/employeeController');
const employeeValidator = require('../controllers/employeeValidator');

router.get('/employee',employeeController.list);
router.get('/employee/add',employeeController.add);
router.post('/employee/save',employeeValidator.addValidator,employeeController.save);
router.get('/employee/del/:id',employeeController.del);
router.get('/employee/delete/:id',employeeController.delete);
router.get('/employee/edit/:id',employeeController.edit);
router.post('/employee/update/:id',employeeValidator.updateValidator,employeeController.update);

module.exports = router;
