const express = require('express');
const router = express.Router();
const documentController = require('../controllers/documentController');
const documentValidator = require('../controllers/documentValidator');

router.get('/document',documentController.list);
router.get('/documenthistory',documentController.listhistory);
router.get('/document/type/:name/:id',documentController.type);
router.get('/document/add',documentController.add);
router.get('/document/addcar',documentController.addcar);

router.post('/document/save',documentValidator.addValidator,documentController.save);

router.get('/document/del/:id',documentController.del);
router.get('/document/delete/:id',documentController.delete);

router.get('/document/delhistory/:id',documentController.delhistory);
router.get('/document/deletehistory/:id',documentController.deletehistory);

router.get('/document/edit/:id',documentController.edit);

router.post('/document/update/:id',documentValidator.UpdateValidator,documentController.update);

router.post('/document/updatepoto/:id',documentController.updatepoto);

module.exports = router;
