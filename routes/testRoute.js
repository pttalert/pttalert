const express = require('express');
const router = express.Router();
const controllerin = require('../controllers/testController');

router.get('/test',controllerin.test);

module.exports = router;
