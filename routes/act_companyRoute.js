const express = require('express');
const router = express.Router();
const act_companyController = require('../controllers/act_companyController');

router.get('/act_company',act_companyController.list);
router.get('/act_company/add',act_companyController.add);
router.post('/act_company/save',act_companyController.save);
router.get('/act_company/edit/:acid',act_companyController.edit);
router.post('/act_company/update/:acid',act_companyController.update);
router.get('/act_company/confirmdelete/:acid',act_companyController.confirmdelete);
router.get('/act_company/delete/:acid',act_companyController.delete);

module.exports = router;
