const express = require('express');
const router = express.Router();

const dochistory = require("../controllers/dochittoryController");
const documentValidator = require('../controllers/documentValidator');

// router.get("/document/dochitory/:id",dochitory.dochittory);
router.get("/dochistory/dochistoryAdd/:id",dochistory.dochistoryAdd)
router.post("/dochitory/dochistorysave",dochistory.dochistorySave)
router.get("/dochistory/dochistoryList",dochistory.dochistoryList)
router.get("/dochistory/del/:id",dochistory.dochistoryDel)
router.get("/dochistory/delete/:id",dochistory.dochistoryDelete)

module.exports = router;
