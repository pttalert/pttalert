const express = require('express');
const router = express.Router();
const typedocumentController = require('../controllers/typedocumentController');
const typedocumentValidator = require('../controllers/typedocumentValidator');

router.get('/typedocument',typedocumentController.list);
router.get('/typedocument/add',typedocumentController.add);
router.post('/typedocument/save',typedocumentValidator.addValidator,typedocumentController.save);
router.get('/typedocument/del/:id',typedocumentController.del);
router.get('/typedocument/delete/:id',typedocumentController.delete);
router.get('/typedocument/edit/:id',typedocumentController.edit);
router.post('/typedocument/update/:id',typedocumentValidator.updateValidator,typedocumentController.update);

module.exports = router;
