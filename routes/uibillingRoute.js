const express = require('express');
const router = express.Router();
const billingController = require('../controllers/billingController');
const billingValidator = require('../controllers/billingValidator');

router.get('/billing',billingController.list);
router.get('/billing/add',billingController.add);
router.post('/billing/save',billingValidator.addValidator,billingController.save);
router.get('/billing/del/:id',billingController.del);
router.get('/billing/delete/:id',billingController.delete);
router.get('/billing/edit/:id',billingController.edit);
router.post('/billing/update/:id',billingValidator.updateValidator,billingController.update);

module.exports = router;
