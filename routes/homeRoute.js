const express = require('express');
const router = express.Router();
const homeController = require('../controllers/homeController');

router.get('/home',homeController.list);
router.get('/home/even/:year/:month/:date',homeController.listdate);

module.exports = router;
