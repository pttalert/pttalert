const express = require('express');
const router = express.Router();
const insuranceController = require('../controllers/insuranceController');

router.get('/insurance/list/:caid',insuranceController.list);
router.post('/insurance/save',insuranceController.save);
router.post('/insurance/update/:caid/:inid',insuranceController.update);
router.get('/insurance/delete/:caid/:inid',insuranceController.delete);

router.get('/insurance/page/edit/:caid/:inid',insuranceController.edit);
router.post('/insurance/page/update/:caid/:inid',insuranceController.update);
router.get('/insurance/page/confirmdelete/:caid/:inid',insuranceController.confirmdelete);
router.get('/insurance/page/delete/:caid/:inid',insuranceController.delete);

module.exports = router;
