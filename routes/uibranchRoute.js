const express = require('express');
const router = express.Router();
const branchController = require('../controllers/branchController');
const branchValidator = require('../controllers/branchValidator');

router.get('/branch',branchController.list);
router.get('/branch/add',branchController.add);
router.post('/branch/save',branchValidator.addValidator,branchController.save);
router.get('/branch/del/:id',branchController.del);
router.get('/branch/delete/:id',branchController.delete);
router.get('/branch/edit/:id',branchController.edit);
router.post('/branch/update/:id',branchValidator.updateValidator,branchController.update);

module.exports = router;
