const express = require('express');
const router = express.Router();
const employeeaccountController = require('../controllers/employeeaccountController');
const employeeaccountValidator = require('../controllers/employeeaccountValidator');

router.get('/employeeaccount',employeeaccountController.list);
router.get('/employeeaccount/add',employeeaccountController.add);
router.post('/employeeaccount/save',employeeaccountValidator.addValidator,employeeaccountController.save);
router.get('/employeeaccount/del/:id',employeeaccountController.del);
router.get('/employeeaccount/delete/:id',employeeaccountController.delete);
router.get('/employeeaccount/edit/:id',employeeaccountController.edit);
router.post('/employeeaccount/update/:id',employeeaccountValidator.updateValidator,employeeaccountController.update);

module.exports = router;
