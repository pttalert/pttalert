const express = require('express');
const router = express.Router();
const controllerf = require('../controllers/LoginController');
const validatorcontrollerf = require('../controllers/LoginValidator');

router.get('/',controllerf.loginForm);
router.post('/login',validatorcontrollerf.loginValidator,controllerf.login);
router.get('/logout',controllerf.logoutt);

router.get('/testlogin',controllerf.logintest1);

module.exports = router;
