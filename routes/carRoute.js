const express = require('express');
const router = express.Router();
const carController = require('../controllers/carController');

router.get('/car',carController.list);
router.get('/car/add',carController.add);
router.post('/car/save',carController.save);
router.get('/cardetail/:caid',carController.detail);
router.get('/car/edit/:caid',carController.edit);
router.post('/car/update/:caid',carController.update);
router.get('/car/confirmdelete/:caid',carController.confirmdelete);
router.get('/car/delete/:caid',carController.delete);

module.exports = router;
