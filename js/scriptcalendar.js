
var getdata1 = <%- JSON.stringify(data1) %> ;
var gatdata3 = [{ddd:"16/11/2563"}];

function generate_year_range(start, end) {
  var years = "";
  for (var year = start; year <= end; year++) {
      years += "<option value='" + year + "'>" + parseInt(year+543) + "</option>";
  }
  return years;
}

var today = new Date();
var currentMonth = today.getMonth();
var currentYear = today.getFullYear();
var selectYear = document.getElementById("year");
var selectMonth = document.getElementById("month");


var createYear = generate_year_range(1970, 2102);
/** or
* createYear = generate_year_range( 1970, currentYear );
*/

document.getElementById("year").innerHTML = createYear;

var calendar = document.getElementById("calendar");
var lang = calendar.getAttribute('data-lang');

var months = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคน", "พฤศจิกายน", "ธันวาคม"];
var days = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"];

var dayHeader = "<tr>";
for (day in days) {
  dayHeader += "<th data-days='" + days[day] + "'>" + days[day] + "</th>";
}
dayHeader += "</tr>";

document.getElementById("thead-month").innerHTML = dayHeader;


monthAndYear = document.getElementById("monthAndYear");
showCalendar(currentMonth, currentYear);



function next() {
  currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
  currentMonth = (currentMonth + 1) % 12;
  showCalendar(currentMonth, currentYear);
}

function previous() {
  currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
  currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
  showCalendar(currentMonth, currentYear);
}

function jump() {
  currentYear = parseInt(selectYear.value);
  currentMonth = parseInt(selectMonth.value);
  showCalendar(currentMonth, currentYear);
}

function showCalendar(month, year) {

  var firstDay = ( new Date( year, month ) ).getDay();

  tbl = document.getElementById("calendar-body");


  tbl.innerHTML = "";


  monthAndYear.innerHTML = months[month] + " " + parseInt(year+543);
  selectYear.value = year;
  selectMonth.value = month;

  // creating all cells

  var date = 1;
  for ( var i = 0; i < 6; i++ ) {
      var row = document.createElement("tr");

      for ( var j = 0; j < 7; j++ ) {
          if ( i === 0 && j < firstDay ) {
              cell = document.createElement( "td" );
              cellText = document.createTextNode("");
              cell.appendChild(cellText);
              row.appendChild(cell);
          } else if (date > daysInMonth(month, year)) {
              break;
          } else {
              cell = document.createElement("td");
              cell.setAttribute("data-date", date);
              if(date<10){
                date="0"+date.toString();
              }
              cell.setAttribute("data-month", month + 1);
              if(parseInt(month+1)<10){
                month ="0"+(parseInt(month+1)).toString();
              }else if(parseInt(month+1)>=10){
                month = parseInt(month+1);
              }
              cell.setAttribute("data-year", year);
              cell.setAttribute("data-month_name", months[month]);
              cell.className = "date-picker";

              var checka = 0;
              var adv = 0;
              for (var k = 0; k < getdata1.length; k++) {
                if (getdata1[k].ddd.trim() == (date+"/"+month+"/"+parseInt(year+543)).toString().trim()) {
                  checka = 1;
                  adv = adv+1;

                }
              }

              if (checka == 1) {
                cell.innerHTML = "<a href='/home/even/"+year+"/"+month +"/"+date+"'  class='notification'> <span>" + date + "</span><span class='badge blink_me'>"+adv+"</span></a>";
              }else {
                cell.innerHTML = "<a href='/home/even/"+year+"/"+month+"/"+date+"'> <span>" + date + "</span></a>";
              }

              if ( date === today.getDate() && year === today.getFullYear() && month === today.getMonth() ) {
                  cell.className = "date-picker selected";
              }
              row.appendChild(cell);
              month = (month-1);
              date++;


          }


      }

      tbl.appendChild(row);
  }

}


function daysInMonth(iMonth, iYear) {
  return 32 - new Date(iYear, iMonth, 32).getDate();
}
